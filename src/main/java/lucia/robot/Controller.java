package lucia.robot;

import java.util.Optional;

/**
 * Relays commands to the drive train.
 *
 * @author lucia
 */

public class Controller {

  private final TrainRpi train;

  public Controller(TrainRpi train) {

    this.train = train;
  }

  public void exec(Command c) {

    switch(c.cmd()) {
      case FORWARD:
        mayBeSpeed(c.arg());
        train.forward();
        break;

      case REVERSE:
        mayBeSpeed(c.arg());
        train.reverse();
        break;

      case RIGHT:
        mayBeSpeed(c.arg());
        train.right();
        break;

      case LEFT:
        mayBeSpeed(c.arg());
        train.left();
        break;

      case SPEED:
        train.speed(c.arg().get());
        break;

      case STOP:
        train.stop();
        break;

      case QUIT:
        System.out.println("Exiting now.");
        System.exit(0);
        break;

      default:
        throw new RuntimeException("Unsupported command: " + c.cmd());
    }
  }

  private void mayBeSpeed(Optional<Integer> v) {

    if(v.isPresent())
      train.speed(v.get());
  }
}
