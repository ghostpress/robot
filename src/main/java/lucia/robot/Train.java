package lucia.robot;

/**
 * Abstract representation of a drivetrain for different controllers.
 *
 * @author lucia
 */

public interface Train {

  void stop();
  void forward();
  void left();
  void right();
  void reverse();
  void speed(int v);
  Thread shutdownHook();
}
