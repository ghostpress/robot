package lucia.robot;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

//@formatter off

/**
 *
 * Models exactly one motor whose direction of rotation is determined by
 * the following table:
 *
 *                   ====================
 *                   inA   inB  Direction
 *                   --------------------
 *                    H     L   forward
 *                    L     H   reverse
 *                    L     L   stop
 *                    H     H   stop
 *                   ====================
 *
 * inA: pre-configured instance of GpioPinDigitalOutput
 * inB: pre-configured instance of GpioPinDigitalOutput
 *
 * @author lucia
 */

public class Motor {

  private final String name;
  private final GpioPinDigitalOutput inA;
  private final GpioPinDigitalOutput inB;

  public Motor() {

    name = null;
    inA = null;
    inB = null;
  }

  public Motor(String name, GpioPinDigitalOutput inA, GpioPinDigitalOutput inB) {

    this.name = name;
    this.inA = inA;
    this.inB = inB;
  }

  // removes power from the motor so it coasts to a stop

  public void stop() {

    inA.low();
    inB.low();
  }

  // sends power into the motor in one direction (+,-)

  public void forward() {

    inA.high();
    inB.low();
  }

  // sends power into the motor in the opposite direction (-,+)

  public void reverse() {

    inA.low();
    inB.high();
  }
}
