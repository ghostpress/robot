package lucia.robot;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinPwm;
import com.pi4j.io.gpio.GpioPinPwmOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

/**
 * System of motors: left bonded, right bonded.
 *
 * Models the drive-train, using the default Pi4j (WiringPi) port numbers:
 *
 *    ======================
 *    Port  Header  H-bridge
 *    ----------------------
 *     0      11      in1
 *     1      12      hardware PWM
 *     2      13      in2
 *     3      15      in3
 *     4      16      in4
 *     5      18      software PWM
 *    ======================
 *
 * L298N dual H-bridge:
 *
 *   Right motor:                    Left motor:
 *
 *   =========================       =========================
 *   enA  in1  in2  out1  out2       enB  in3  in4  out3  out4
 *   -------------------------       -------------------------
 *    H    H    L    H     L          H    H    L    H     L
 *    H    L    H    L     H          H    L    H    L     H
 *    H    L    L    <break>          H    L    L    <break>
 *    H    H    H    <break>          H    H    H    <break>
 *    L   any  any   <free>           L   any  any   <free>
 *   =========================       =========================
 *
 * @author lucia
 *
 */

public class TrainRpi implements Train {

  private final GpioController gpio;

  private final GpioPinDigitalOutput rInA;
  private final GpioPinDigitalOutput rInB;
  private final GpioPinDigitalOutput lInA;
  private final GpioPinDigitalOutput lInB;
  private final GpioPinPwmOutput     pwmSpeed;

  private final Motor rMotor;
  private final Motor lMotor;

  private final int    PWM_RANGE_REAL = 100; // rpi constant
  private final int    PWM_RANGE_CMD  = 10; // scaled down for less typing
  private final float  PWM_RATIO      = PWM_RANGE_REAL/PWM_RANGE_CMD;

  public TrainRpi(GpioController gpio) {

    this.gpio = gpio;

    rInA = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00,"h-bridge-in1", PinState.LOW);
    rInB = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02,"h-bridge-in1", PinState.LOW);
    lInA = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03,"h-bridge-in1", PinState.LOW);
    lInB = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04,"h-bridge-in1", PinState.LOW);

    lMotor = new Motor("right", rInA, rInB);
    rMotor = new Motor("left",  lInA, lInB);

    pwmSpeed = gpio.provisionSoftPwmOutputPin(RaspiPin.GPIO_05, "pwm-speed", 0); // starts at speed 0
    pwmSpeed.setPwmRange(PWM_RANGE_REAL); // set maximum speed

    // Reset all pins on shutdown

    rInA.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
    rInB.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
    lInA.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
    lInB.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
    pwmSpeed.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

  }

  // used in other commands to keep the H-bridge from shorting

  public void stop() {
    lMotor.stop();
    rMotor.stop();
  }

  public void forward() {
    stop();
    lMotor.forward();
    rMotor.forward();
  }

  // turn left

  public void left() {
    stop();
    lMotor.reverse();
    rMotor.forward();
  }

  // turn right

  public void right() {
    stop();
    lMotor.forward();
    rMotor.reverse();
  }

  public void reverse() {
    stop();
    lMotor.reverse();
    rMotor.reverse();
  }

  public void speed(int v) {
    // negative sign is redundant, since reverse() is implemented, and ensure that the desired speed is within range

    int percent = Math.round( PWM_RATIO * Math.min( Math.abs(v), PWM_RANGE_CMD ));
    pwmSpeed.setPwm(percent);
  }

  public Thread shutdownHook() {
    return new Thread(new Shutdown(gpio));
  }
}

