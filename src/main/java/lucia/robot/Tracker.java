package lucia.robot;

import java.util.Map;
import java.util.HashMap;

/**
 * Recalls the commands given after the "TRACK" command:
 * Reverse, stop, forward, etc., mapped with the time for which each action lasted.
 *
 * TODO: implement timekeeper?
 *
 * Stores them into ArrayList directions to repeat them autonomously.
 *
 * @author lucia
 */

public class Tracker {

  private static Map<Command, Double> directions = new HashMap<Command, Double>();

  // TODO: finish

}
