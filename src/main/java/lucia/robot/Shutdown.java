package lucia.robot;

import com.pi4j.io.gpio.GpioController;

/**
 * Runs alongside Main class.
 * Ensures that pi & gpio have shut down safely.
 *
 * @author lucia
 */

public class Shutdown implements Runnable {

  private final GpioController gpio;

  public Shutdown(GpioController gpio) {

    this.gpio = gpio;
  }

  public void run() {

    gpio.shutdown();
  }

}
