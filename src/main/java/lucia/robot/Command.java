package lucia.robot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Enumerated commands for controlling the robot:
 * String followed by spaces, followed by Integers (optional for speed/time controls).
 *
 * @author lucia
 */

public class Command {

  // subject to change for optimization
  public enum Cmd {
    FORWARD("f"),
    REVERSE("b"),
    LEFT("l"),
    RIGHT("r"),
    SPEED("v"),
    STOP("s"),
    QUIT("q");

    private final String keyword;
    private static final Map <String, Cmd> commands = new HashMap<String, Cmd>();

    Cmd(String keyword) {

      String k = keyword.toLowerCase();

      this.keyword = k;
    }

    public String keyword() { return keyword; }

    public static Cmd parse(String text) {

      Cmd c = commands.get(text.toLowerCase());

      if(c == null)
        throw new RuntimeException("Unsupported command keyword: " + text);

      return c;
    }

    static {

      for(Cmd c : Cmd.values()) {

        // prevent different commands from having the same keyword

        if(commands.keySet().contains(c.keyword()))
          throw new RuntimeException("Keyword already in use: " + c.keyword());

        // save the keyword so it doesn't get used again with a different command later

        commands.put(c.keyword(), c);
      }
    }
  }

  private final Cmd cmd;

  private final Optional<Integer> arg; // argument: speed, direction, time, etc., not always needed

  public Command(Cmd cmd, Optional<Integer> arg) {

    this.cmd = cmd;
    this.arg = arg;

    // SPEED requires an Integer argument

    if(cmd == Cmd.SPEED && !arg.isPresent())
      throw new RuntimeException("Unspecified speed: " + cmd + "; please include argument");
  }

  public Optional<Integer> arg() { return arg; }

  public Cmd cmd() { return cmd; }

  // text: a String containing a regular expression (in this case, spaces)
  public static Command parse(String text) {

    if(text == null || text.length() == 0)
      throw new RuntimeException("Unspecified command to parse");

    String[] tokens = text.split("\\s+"); // looks for one or more spaces within the String

    switch(tokens.length) {

      case 1: // no argument
        Cmd cmd = Cmd.parse(tokens[0]);

        Optional<Integer> arg = Optional.empty();
        return new Command( cmd, arg );

      case 2: // two words: one String, one argument
        Cmd cmd2 = Cmd.parse(tokens[0]);
        Optional<Integer> arg2 = Optional.of(Integer.parseInt(tokens[1]));
        return new Command( cmd2, arg2 );

      default:
        throw new RuntimeException("Unsupported command: " + text + "; too many tokens");
    }
  }
}
