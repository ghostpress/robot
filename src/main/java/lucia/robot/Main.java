package lucia.robot;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioController;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * @author lucia
 */

public class Main {

  private static final String CMDS = "Commands: f, b, l, r, v, s, q";

  public static void main(String[] args) {

    // get instance of gpiocontroller, complete with pins
    GpioController gpio = GpioFactory.getInstance();
    TrainRpi train      = new TrainRpi(gpio);

    // calls gpio.shutdown() in the run() method of Shutdown class, just before JVM exits
    Runtime.getRuntime().addShutdownHook( train.shutdownHook() );

    Controller controller = new Controller(train);
    Scanner    reader     = new Scanner(System.in);

    System.out.println(CMDS);

    while(true) {

      try {
        String line = reader.nextLine();
        controller.exec(Command.parse(line));
      }
      catch(NoSuchElementException e) { System.exit(0); }
      catch(Throwable              t) { System.out.println("Error: " + t); }
    }
  }
}
