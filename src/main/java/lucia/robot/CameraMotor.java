package lucia.robot;

/**
 * Class for controlling the movement of the camera motor.
 *
 * TODO: documentation
 *
 * @author lucia
 */

import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class CameraMotor extends Motor {

  private final String name;
  private final GpioPinDigitalOutput cam;

  private double degrees;


  public CameraMotor(String name, GpioPinDigitalOutput cam) {

    this.name = name;
    this.cam = cam;
  }

  public void stop() {

    // TODO: implement
  }

  // pan clockwise
  public void forward() {

    // TODO: implement
  }

  // pan counterclockwise
  public void reverse() {

    // TODO: implement
  }

  public void pan(double degrees) {

    // TODO: implement
  }

}
