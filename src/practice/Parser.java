/**
 * Parses a String of characters (letters, followed by one or two spaces,
 * followed by numbers) for the numbers.
 *
 * @author lucia
 */

public class Parser {

  public static void main(String[] args) {

    String str1 = "jsgfydsg 4598485";
    String str2 = "jhfkj  09";
    String str3 = "";
    String str4 = " ";
    String str5 = "jbhjhb";
    String str6 = "";

    String[] strings = {str1, str2, str3, str4, str5, str6};

    try {

      for(int i = 0; i < strings.length; i++) {

        parse(strings[i]);
      }

    } catch(NumberFormatException n) {

      n = new NumberFormatException("Please check that the String contains an integer.");

      throw n;
    }
  }

  /**
   * @param toParse   a String containing numbers to be parsed
   * @return parsed   the Integers parsed
   */

  public static Integer parse(String toParse) {

    Integer parsed;

    Integer index = indexOfInt(toParse);

    String nums = "";

    while(index > -1) {

      for (int i = index; i < toParse.length() - 1; i++) {

        nums += toParse.substring(i, i + 1);

      }
    }

    parsed = Integer.parseInt(nums);

    return parsed;

  }

  private static int indexOfInt(String str) {

    int index = -1;

    for(int i = 0; i < str.length() - 3; i++) {

      if(str.substring(i, i+1).equals(" ")) {

        if(str.substring(i+2, i+3).equals(" ")) {

          index = i + 4;
        } else {

          index = i + 3;
        }
      }
    }

    return index;

  }

}
