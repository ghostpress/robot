import java.util.ArrayList;

/**
 * Created by lucia on 5/14/17.
 *
 * Enum & switch{} practice.
 *
 */

public class Calendar {

  Day day;

  private enum Day {

    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY

  }

  public Calendar(Day day) {

    this.day = day;

  }

  public void whichDay() {

    switch(day) {

      case SUNDAY:
        System.out.println("Have you finished all your homework?");
        break;

      case MONDAY:
        System.out.println("Gross");
        break;

      case TUESDAY:
        System.out.println("One down...");
        break;

      case WEDNESDAY:
        System.out.println("Halfway there!");
        break;

      case THURSDAY:
        System.out.println("Almost...");
        break;

      case FRIDAY:
        System.out.println("Finally!");
        break;

      case SATURDAY:
        System.out.println("Sleep in and relax");
        break;
    }
  }

  public static void main(String[] args) {

    ArrayList<Calendar> days = new ArrayList<Calendar>();

    Calendar dayOne = new Calendar(Day.MONDAY);
    days.add(dayOne);

    Calendar dayTwo = new Calendar(Day.TUESDAY);
    days.add(dayTwo);

    Calendar dayThree = new Calendar(Day.WEDNESDAY);
    days.add(dayThree);

    Calendar dayFour = new Calendar(Day.THURSDAY);
    days.add(dayFour);

    Calendar dayFive = new Calendar(Day.FRIDAY);
    days.add(dayFive);

    Calendar daySix = new Calendar(Day.SATURDAY);
    days.add(daySix);

    Calendar daySeven = new Calendar(Day.SUNDAY);
    days.add(daySeven);

    for(Calendar c: days)
      c.whichDay();

  }



}
